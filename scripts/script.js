function validarFormulario() {
  var nome = document.forms["formulario"]["nome"].value;
  var email = document.forms["formulario"]["email"].value;
  var mensagem = document.forms["formulario"]["mensagem"].value;
  if (nome == "" || email == "" || mensagem == "") {
    alert("Por favor, preencha todos os campos.");
    return false;
  }
  return true;
}

document.getElementById("contato").onclick = function () {
  location.href = "fale-conosco.html";
};

document.getElementById("main").onclick = function (event) {
  event.preventDefault();
  location.href = "index.html";
};

document.getElementById("login").onclick = function (event) {
  event.preventDefault();
  location.href = "login.html";
};

document.getElementById("biografia").onclick = function (event) {
  event.preventDefault();
  location.href = "biografia.html";
};

function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

function Login() {
  event.preventDefault();
  const email = document.getElementById("email").value;
  const password = document.getElementById("password").value;

  if (!email || !password) {
    alert("Por favor, preencha todos os campos!");
    return;
  }

  firebase
    .auth()
    .signInWithEmailAndPassword(email, password)
    .then((userCredential) => {
      console.log("Usuário autenticado com sucesso!");
      console.log(userCredential.user);
      localStorage.setItem("userAuthenticated", "true");
      window.location.href = "biografia.html";
    })
    .catch((error) => {
      var errorCode = error.code;
      var errorMessage = error.message;
      alert(
        "Problemas ao autenticar usuário. Verifique se preencheu tudo corretamente!"
      );
      console.log(error.message);
    });
}

function cadastraBiografia() {
  event.preventDefault();
  console.log("Script cadastra biografia");

  const Timestamp = firebase.firestore.Timestamp;

  const nome = document.getElementById("nome").value;
  const dataNascimentoInput = document.getElementById("dataNascimento");
  const dataFalecimentoInput = document.getElementById("dataFalecimento");
  const dataNascimentoString = dataNascimentoInput.value;
  const dataFalecimentoString = dataFalecimentoInput.value;
  const dataNascimento = Timestamp.fromDate(new Date(dataNascimentoString));
  const dataFalecimento = Timestamp.fromDate(new Date(dataFalecimentoString));
  const biografia = document.getElementById("biografia").value;
  //const imagemUrl = document.getElementById("imagemUrl").value;
 // const imagemArquivo = document.getElementById("imagemArquivo").files[0];

  if (nome === "") {
    alert("Por favor, preencha o campo Nome.");
    return;
  }

  if (biografia === "") {
    alert("Por favor, preencha o campo Biografia.");
    return;
  }

 // if (imagemUrl !== "" && imagemArquivo) {
  //  alert("Por favor, selecione apenas uma opção de envio de imagem.");
 //   return;
 // }

  //if (imagemUrl !== "") {
    // Opção 1: Utilizando a URL de um arquivo na internet
   // console.log("URL da imagem:", imagemUrl);
    // Faça o que for necessário com a URL da imagem
  //} else if (imagemArquivo) {
    // Opção 2: Realizando o upload localmente
    //enviarImagem(imagemArquivo)
      //.then((urlImagem) => {
        //console.log("URL de download da imagem:", urlImagem);
        // Faça o que for necessário com a URL de download da imagem

        // Depois de obter a URL da imagem, você pode prosseguir com o envio dos dados do formulário para o Firebase Firestore
        enviarFormulario();
     // })
      //.catch((error) => {
      //  console.log("Erro ao enviar a imagem:", error);
     // });
  //} else {
    // Nenhuma opção de envio de imagem selecionada
   // alert("Por favor, selecione uma opção de envio de imagem.");
 // }
}

function enviarFormulario() {
  const nome = document.getElementById("nome").value;
  const dataNascimento = document.getElementById("dataNascimento").value;
  const dataFalecimento = document.getElementById("dataFalecimento").value;
  const biografia = document.getElementById("biografia").value;

  firebase
    .firestore()
    .collection("biografia")
    .add({
      nome: nome,
      dataNascimento: dataNascimento,
      dataFalecimento: dataFalecimento,
      biografia: biografia,
    })
    .then(function (docRef) {
      console.log("Documento cadastrado com ID: ", docRef.id);
      alert("Cadastro realizado com sucesso!");
      // Redirecionar ou executar ação adicional após o cadastro
    })
    .catch(function (error) {
      console.error("Erro ao cadastrar documento: ", error);
      alert(
        "Ocorreu um erro ao realizar o cadastro. Por favor, tente novamente."
      );
    });
}

// function enviarImagem(imagem) {
//   return new Promise((resolve, reject) => {
//     if (typeof imagem === "string") {
//       // Se a imagem for uma URL de arquivo na internet
//       resolve(imagem);
//     } else if (imagem instanceof File) {
//       // Se a imagem for um arquivo local

//       // Crie uma referência de armazenamento no Firebase Firestore
//       var storageRef = firebase.storage().ref();

//       // Crie um nome único para a imagem, por exemplo, com base no timestamp atual
//       var nomeImagem = new Date().getTime() + "_" + imagem.name;

//       // Defina o caminho do arquivo dentro do armazenamento do Firebase
//       var caminhoImagem = "imagens/" + nomeImagem;

//       // Faça o upload do arquivo para o Firebase Storage
//       var uploadTask = storageRef.child(caminhoImagem).put(imagem);

//       // Monitore o progresso do upload
//       uploadTask.on(
//         "state_changed",
//         function (snapshot) {
//           // Acompanhe o progresso do upload aqui, se necessário
//         },
//         function (error) {
//           // Lidar com erros durante o upload da imagem
//           console.log("Erro ao enviar a imagem:", error);
//           reject(error);
//         },
//         function () {
//           // O upload da imagem foi concluído com sucesso
//           console.log("Imagem enviada com sucesso!");

//           // Obtenha a URL de download da imagem do Firebase Storage
//           uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
//             // Resolva a Promise com a URL de download da imagem
//             resolve(downloadURL);
//           });
//         }
//       );
//     } else {
//       // Tipo de entrada inválido
//       reject(new Error("Tipo de imagem inválido."));
//     }
//   });
// }

function enviaMensagem() {
  event.preventDefault();
  console.log("Script envia mensagem.");

  const nome = document.getElementById("nome").value;
  const email = document.getElementById("email").value;
  const mensagem = document.getElementById("mensagem").value;

  if (nome === "") {
    alert("Por favor, preencha o campo Nome.");
    return;
  }

  if (email === "") {
    alert("Por favor, preencha o campo E-mail.");
    return;
  }

  if (mensagem === "") {
    alert("Por favor, preencha o campo Mensagem.");
    return;
  }
  enviarMensagem();
}

function enviarMensagem() {
  const nome = document.getElementById("nome").value;
  const email = document.getElementById("email").value;
  const mensagem = document.getElementById("mensagem").value;

  firebase
    .firestore()
    .collection("faleconosco")
    .add({
      nome: nome,
      email: email,
      mensagem: mensagem,
    })
    .then(function (docRef) {
      console.log("Documento cadastrado com ID: ", docRef.id);
      alert("Cadastro realizado com sucesso!");
      // Redirecionar ou executar ação adicional após o cadastro
    })
    .catch(function (error) {
      console.error("Erro ao cadastrar documento: ", error);
      alert(
        "Ocorreu um erro ao realizar o cadastro. Por favor, tente novamente."
      );
    });

  document.getElementById("nome").value = "";
  document.getElementById("email").value = "";
  document.getElementById("mensagem").value = "";

  return true;
}
